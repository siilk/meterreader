// Copyright Red Energy Limited 2017
// Created by Dmitry Vladimirov

package main.java.simplenem12.parser;

import main.java.simplenem12.entity.EnergyUnit;
import main.java.simplenem12.entity.MeterRead;
import java.util.List;
import java.util.function.Function;

/**
 * function which creates MeterRead object from a list of raw string values
 */
public class MeterReadExtractor implements Function<List<String>, MeterRead> {

    public static final int INDEX_RECORD_TYPE = 0;
    public static final int INDEX_NMI = 1;
    public static final int INDEX_ENERGY_UNIT = 2;

    public static final int EXPECTED_FIELD_COUNT = 3;

    @Override
    public MeterRead apply(List<String> rawBlock) {
        if (rawBlock.size() != EXPECTED_FIELD_COUNT) {
            throw new SimpleNem12ParsingException("Incorrect file format, MR block start must have 3 elements");
        }
        if ( ! rawBlock.get(INDEX_RECORD_TYPE).equals(SimpleNem12Parser.NEM_12_RECORD_TYPE_BLOCK_START)) {
            throw new SimpleNem12ParsingException("Incorrect file format, invalid MR block header");
        }
        EnergyUnit energyUnit;
        try
        {
            energyUnit = EnergyUnit.valueOf(rawBlock.get(INDEX_ENERGY_UNIT));
        }
        catch(IllegalArgumentException ex) {
            throw new SimpleNem12ParsingException(ex);
        }
        return new MeterRead(rawBlock.get(INDEX_NMI), energyUnit);
    }
}