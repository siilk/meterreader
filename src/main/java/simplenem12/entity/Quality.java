// Copyright Red Energy Limited 2017

package main.java.simplenem12.entity;

/**
 * Represents meter read quality in SimpleNem12
 */
public enum Quality {
  A, E
}
