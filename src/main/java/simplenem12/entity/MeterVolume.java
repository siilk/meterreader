// Copyright Red Energy Limited 2017

package main.java.simplenem12.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Represents a meter volume with quality, values should come from RecordType 300
 */
public class MeterVolume {

    private LocalDate localDate;
    private BigDecimal volume;
    private Quality quality;

    public MeterVolume(BigDecimal volume, Quality quality) {
        this(volume, quality, null);
    }
    
    public MeterVolume(BigDecimal volume, Quality quality, LocalDate localDate) {
        this.volume = volume;
        this.quality = quality;
        this.localDate = localDate;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MeterVolume that = (MeterVolume) o;
        return Objects.equals(getVolume(), that.getVolume()) &&
               Objects.equals(getLocalDate(), that.getLocalDate()) &&
                getQuality() == that.getQuality();
    }

    public int hashCode() {
        return Objects.hash(getVolume(), getQuality());
    }

    //generated
    @Override
    public String toString()
    {
        return "MeterVolume{" +
            "localDate=" + (localDate == null ? "<null>" : localDate) +
            ", volume=" + (volume == null ? "<null>" : volume) +
            ", quality=" + (quality == null ? "<null>" : quality) +
            '}';
    }
}
