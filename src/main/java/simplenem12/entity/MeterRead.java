// Copyright Red Energy Limited 2017

package main.java.simplenem12.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Represents a meter read, corresponds to RecordType 200 in SimpleNem12
 * <p>
 * The volumes property is a map that holds the date and associated <code>MeterVolume</code> on that date, values come from RecordType 300.
 */
public class MeterRead {

    private String nmi;
    private EnergyUnit energyUnit;
    private SortedMap<LocalDate, MeterVolume> volumes = new TreeMap<>();

    public MeterRead(String nmi, EnergyUnit energyUnit) {
        this.nmi = nmi;
        this.energyUnit = energyUnit;
    }

    public String getNmi() {
        return nmi;
    }

    public void setNmi(String nmi) {
        this.nmi = nmi;
    }

    public EnergyUnit getEnergyUnit() {
        return energyUnit;
    }

    public void setEnergyUnit(EnergyUnit energyUnit) {
        this.energyUnit = energyUnit;
    }

    public SortedMap<LocalDate, MeterVolume> getVolumes() {
        return volumes;
    }

    public void setVolumes(SortedMap<LocalDate, MeterVolume> volumes) {
        this.volumes = volumes;
    }

    MeterVolume getMeterVolume(LocalDate localDate) {
        return volumes.get(localDate);
    }

    public void appendVolume(MeterVolume meterVolume) {
        appendVolume(meterVolume.getLocalDate(), meterVolume);
    }
    
    public void appendVolume(LocalDate localDate, MeterVolume meterVolume) {
        volumes.put(localDate, meterVolume);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MeterRead meterRead = (MeterRead) o;
        return Objects.equals(getNmi(), meterRead.getNmi());
    }

    public int hashCode() {
        return Objects.hash(getNmi());
    }

    public BigDecimal getTotalVolume() {
        return volumes.values().stream().map(MeterVolume::getVolume)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    //generated
    @Override
    public String toString()
    {
        return "MeterRead{\n" +
            "nmi='" + (nmi == null ? "<null>" : nmi) + '\'' +
            ", energyUnit=" + (energyUnit == null ? "<null>" : energyUnit) +
            (volumes == null ? "" : ("\n" + String.join(",\n", volumes.values().stream().map(MeterVolume::toString)
            .collect(Collectors.toCollection(ArrayList::new))))) + '}';
    }
}
