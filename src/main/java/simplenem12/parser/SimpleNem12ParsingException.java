// Copyright Red Energy Limited 2017
// Created by Dmitry Vladimirov

package main.java.simplenem12.parser;

/**
 * Exception which could be throw by SimpleNem12Parser implementations  
 */
public class SimpleNem12ParsingException extends RuntimeException {
    
    public SimpleNem12ParsingException() {
        super();
    }

    public SimpleNem12ParsingException(String message) {
        super(message);
    }

    public SimpleNem12ParsingException(Exception cause) {
        super(cause);
    }
}
