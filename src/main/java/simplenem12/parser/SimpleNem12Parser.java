// Copyright Red Energy Limited 2017

package main.java.simplenem12.parser;

import main.java.simplenem12.entity.MeterRead;

import java.io.File;
import java.util.Collection;

public interface SimpleNem12Parser {

    String NEM_12_RECORD_TYPE_HEADER = "100";
    String NEM_12_RECORD_TYPE_BLOCK_START = "200";
    String NEM_12_RECORD_TYPE_VOLUME_DATA = "300";
    String NEM_12_RECORD_TYPE_FOOTER = "900";
  
    /**
     * Parses Simple NEM12 file.
     *
     * @param simpleNem12File file in Simple NEM12 format
     * @throws SimpleNem12ParsingException
     * @return Collection of <code>MeterRead</code> that represents the data in the given file.
     */
    Collection<MeterRead> parseSimpleNem12(File simpleNem12File) throws SimpleNem12ParsingException;

}
