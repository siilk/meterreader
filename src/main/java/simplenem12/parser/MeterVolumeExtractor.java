// Copyright Red Energy Limited 2017
// Created by Dmitry Vladimirov

package main.java.simplenem12.parser;

import main.java.simplenem12.entity.MeterVolume;
import main.java.simplenem12.entity.Quality;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

/**
 * function which creates MeterVolume object from a list of raw string values
 */
public class MeterVolumeExtractor implements Function<List<String>, MeterVolume> {
    public static final int INDEX_RECORD_TYPE = 0;
    public static final int INDEX_TIMESTAMP = 1;
    public static final int INDEX_VOLUME = 2;
    public static final int INDEX_QUALITY = 3;

    public static final int BEGIN_INDEX_YEAR = 0;
    public static final int END_INDEX_YEAR = 4;
    public static final int BEGIN_INDEX_MONTH = 4;
    public static final int END_INDEX_MONTH = 6;
    public static final int BEGIN_INDEX_DAY = 6;
    public static final int END_INDEX_DAY = 8;

    public static final int EXPECTED_FIELD_COUNT = 4;
    public static final int EXPECTED_DATE_LENGTH = 8;

    public static final char DECIMAL_SEPARATOR = '.';
    public static final String NUMBER_PATTERN = "#0.##";

    @Override
    public MeterVolume apply(List<String> rawData) {
        if (rawData.size() != EXPECTED_FIELD_COUNT) {
            throw new SimpleNem12ParsingException("Incorrect file format, volume record must have 4 elements");
        }
        if ( ! rawData.get(INDEX_RECORD_TYPE).equals(SimpleNem12Parser.NEM_12_RECORD_TYPE_VOLUME_DATA)) {
            throw new SimpleNem12ParsingException("Incorrect file format, invalid volume record header");
        }
        BigDecimal volume = parseVolume(rawData.get(INDEX_VOLUME));
        Quality quality;
        try {
            quality = Quality.valueOf(rawData.get(INDEX_QUALITY));
        }
        catch (IllegalArgumentException ex) {
            throw new SimpleNem12ParsingException("Incorrect file format, invalid quality specified for metered volume record");
        }
        return new MeterVolume(volume, quality, parseDate(rawData.get(INDEX_TIMESTAMP)));
    }

    private LocalDate parseDate(String rawDate) throws SimpleNem12ParsingException{
        if (rawDate == null){
            throw new SimpleNem12ParsingException("Raw date is null");
        }
        if (rawDate.length() != EXPECTED_DATE_LENGTH){
            throw new SimpleNem12ParsingException("Raw date format is not correct");
        }
        try {
            return LocalDate.of(Integer.parseInt(rawDate.substring(BEGIN_INDEX_YEAR, END_INDEX_YEAR)),
                Integer.parseInt(rawDate.substring(BEGIN_INDEX_MONTH, END_INDEX_MONTH)), Integer.parseInt(rawDate.substring(BEGIN_INDEX_DAY, END_INDEX_DAY)));
        }
        catch (NumberFormatException ex){
            throw new SimpleNem12ParsingException(ex);
        }
    }

    private BigDecimal parseVolume(String rawVolume) throws SimpleNem12ParsingException {
        if (rawVolume == null){
            throw new SimpleNem12ParsingException("Raw volume is null");
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(DECIMAL_SEPARATOR);
        DecimalFormat decimalFormat = new DecimalFormat(NUMBER_PATTERN, symbols);
        decimalFormat.setParseBigDecimal(true);
        try {
            return (BigDecimal) decimalFormat.parse(rawVolume);
        }
        catch (ParseException e) {
            throw new SimpleNem12ParsingException(e);
        }
    }
}
