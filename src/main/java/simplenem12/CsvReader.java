// Copyright Red Energy Limited 2017
// Created by Dmitry Vladimirov

package main.java.simplenem12;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Reads a csv file into a list of lists of strings, each list representing a row string values.
 * Rows can be of variable length 
 */
public class CsvReader {
    
    public static List<List<String>> readCSV(File csvFile) throws Exception {
        if (csvFile == null ) {
            throw new IllegalArgumentException("Input file is not set");
        }
        if ( ! csvFile.exists()) {
            throw new FileNotFoundException("Input file does not exist");
        }
        List<List<String>> res = new ArrayList<>();
        List<String> rawData = new ArrayList<>(Files.readAllLines(csvFile.toPath())); //readAllLines might return an unmodifiable list
        if (rawData.size() > 0) {
            //replace() is used to preserve rows with empty columns
            res = rawData.stream().filter(rawDatum -> (rawDatum != null && ! rawDatum.equals("")))
                    .map(rawDatum -> rawDatum.replace(",", " , ").split(",")).filter(rawFields -> rawFields.length > 0)
                    .map(Arrays::asList).map(fieldList -> fieldList.stream().map(String::trim)
                    .collect(Collectors.toCollection(ArrayList::new)))
                    .collect(Collectors.toCollection(ArrayList::new));
        }
        return res;
    }
    
    private CsvReader(){}
}
