// Copyright Red Energy Limited 2017

package main.java.simplenem12.entity;

/**
 * Represents the KWH energy unit in SimpleNem12
 */
public enum EnergyUnit {
  KWH
}
