// Copyright Red Energy Limited 2017
// Created by Dmitry Vladimirov

package main.java.simplenem12.parser;

import main.java.simplenem12.CsvReader;
import main.java.simplenem12.entity.MeterRead;
import main.java.simplenem12.entity.MeterVolume;

import java.io.File;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;

/**
 * Parses SimpleNem12 data from a provided csv file 
 */
public class SimpleNem12ParserImpl implements SimpleNem12Parser {

    /**
     * Implementation of {@link SimpleNem12Parser}. Parses SimpleNem12 data from a provided csv file 
     * @param simpleNem12File file in Simple NEM12 format
     * @return A collection(java.util.List) of MeterRead objects constructed from data in simpleNem12File
     * @throws SimpleNem12ParsingException if file parsing error occurs or if data in the file does not match expected format  
     */
    @Override
    public Collection<MeterRead> parseSimpleNem12(File simpleNem12File) throws SimpleNem12ParsingException {
        List<List<String>> rawData;
        try {
            rawData = CsvReader.readCSV(simpleNem12File);
        }
        catch (Exception e) {
            throw new SimpleNem12ParsingException(e);
        }
        if (rawData == null || rawData.size() == 0) {
            return null;
        }

        rawData = rawData.stream().filter(Objects::nonNull).filter(rawDatum -> rawDatum.size() > 0)
            .collect(Collectors.toCollection(ArrayList::new));
        
        List<String> header = rawData.get(0);
        if ( ! header.get(0).equals(NEM_12_RECORD_TYPE_HEADER)){
            throw new SimpleNem12ParsingException("Incorrect file header");
        }
        rawData.remove(0);
        
        List<String> footer = rawData.get(rawData.size() - 1);
        if ( ! footer.get(0).equals(NEM_12_RECORD_TYPE_FOOTER)){
            throw new SimpleNem12ParsingException("Incorrect file footer");
        }
        rawData.remove(rawData.size() - 1);

        MeterReadConsumer consumer = new MeterReadConsumer();
        rawData.forEach(consumer);
        return consumer.getMeterReads();
    }

    /**
     * MeterReadConsumer is used to transform a list of rows obtained from NEM 12 csv file
     * into a list of MeterRead objects containing MeterVolume records
     */
    private class MeterReadConsumer implements Consumer<List<String>> {
        private List<MeterRead> meterReads = new ArrayList<>();
        private MeterReadExtractor meterReadExtractor = new MeterReadExtractor();
        private MeterVolumeExtractor meterVolumeExtractor = new MeterVolumeExtractor();

        @Override
        public void accept(List<String> row)
        {
            switch (row.get(0)) {
                case NEM_12_RECORD_TYPE_BLOCK_START:
                    meterReads.add(meterReadExtractor.apply(row));
                    break;
                case NEM_12_RECORD_TYPE_VOLUME_DATA:
                    MeterVolume meterVolume = meterVolumeExtractor.apply(row);
                    if (meterReads ==  null || meterReads.size() == 0){
                        throw new SimpleNem12ParsingException("No meter reads to add volume to");
                    }
                    meterReads.get(meterReads.size() - 1).appendVolume(meterVolume);
                    break;
                default:
                    throw new SimpleNem12ParsingException("Invalid record type");
            }
        }

        public List<MeterRead> getMeterReads() {
            return new ArrayList<>(meterReads);
        }
    }
}
